function UsuariosDAO(connection){

	this._connection = connection();
	
	UsuariosDAO.prototype.enviarUsuario = function(usuario, callback){
		console.log('data: ', usuario)
		this._connection.query('insert into usuarios set ?', usuario, callback);
	}
}

module.exports = function(){
	return UsuariosDAO;
}