function ArtigosModels(connection){
   this._connection = connection(); 
    // console.log('db> ', db);
 }

 ArtigosModels.prototype.inserirArtigo = function(dados){
    this._connection.open(function(err, mongoclient){
    
      mongoclient.collection("artigos", function(err, collection){
        
        collection.insert(dados);
        mongoclient.close();
  
      });
  
    });
  };

  module.exports = function(){
    return ArtigosModels;
  }; 